# `> powerline.bash`

`powerline.bash` est une invite de commande rapide dans le style Powerline,
pour BASH.

![capture d'écran](docs/screenshot.png)

Ça, c'est pour le sapin de Noël :-) Au jour le jour, ça ressemble plutôt à ça:

![capture d'écran bersace](docs/screenshot-bersace.png)


## Fonctionalités

- *Installation facile*. Un seul fichier sans dépendances.
- *Très rapide*. Jusqu'à seulement 15ms pour générer l'invite. *Pas de serveur !*
- Sur deux lignes pour faciliter le *copicollage*, garder le curseur au même
  endroit et laisser de la place aux longues commandes.
- `utilisateur@hôte` si dans un client SSH ou sudo, *coloré dynamiquement*.
- Dossier courant avec *dossiers intermédiaires abrégés*.
- Nom du *virtualenv Python* ou version Pyenv.
- Infos *git* : branche courante, modifications non validées, synchronisation
  avec la branche amont.
- En cas de commande en erreur, affichage du code de sortie et coloration du
  `$`.
- *Icônes* optionnelles avec
  [icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal/).
- Édition du *titre de fenêtre* du terminal.
- De nombreux segments disponibles : OpenStack, Maildir, Kubernetes etc.
- *Configurable* par des variables shell. Pas de fichier.
- *Extensible*.


## Installation

``` console
$ curl -Lo ~/.config/powerline.bash https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash
$ $EDITOR ~/.bashrc
# Copier et adapter ceci dans votre .bashrc
. ${HOME}/.config/powerline.bash
PROMPT_COMMAND='__update_ps1 $?'
$ exec $SHELL
```

Les chevrons façon Powerline requièrent une police adaptée
[PowerLineSymbols](https://github.com/powerline/powerline/tree/develop/font) ou
[PowerlineExtraSymbols](https://github.com/ryanoasis/powerline-extra-symbols).
Vous pouvez soit configurer fontconfig pour utiliser la police Powerline, soit
utiliser une police combinée comme
[NerdFont](https://github.com/ryanoasis/nerd-fonts/).

## Configuration

powerline.bash prends en compte ces variables sans recharger le shell. Pas
besoin de les exporter dans l'environnement.

`POWERLINE_SEP` et `POWERLINE_THINSEP`, pour définir les séparateurs de
segments.

![capture d'écran séparateur](docs/screenshot-sep.png)

`POWERLINE_SEGMENTS` liste les infos à afficher. La valeurs par défaut est
calculée à l'initialisation du shell, en fonction de l'environnement.

`POWERLINE_WINDOW_TITLE` définit le titre de la fenêtre ou de l'onglet (par
exemple `POWERLINE_WINDOW_TITLE="\\h"` affiche le nom de l'hôte).

`POWERLINE_MAILDIR` définit le répertoire Maildir à surveiller (le répertoire
effectivement monitoré sera `$POWERLINE_MAILDIR/new`).

Pour aller plus loin dans la configuration et la personnalisation de
`powerline.bash`, le [cookbook](docs/COOKBOOK.md) détaille le choix
d'une police, des icônes, le configuration de votre terminal préféré,
et bien plus...

## Icônes

Pour avoir des icônes dans son shell, il faut:

- Installer [icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal/).
- Définir `POWERLINE_ICONS=icons-in-terminal` avant de sourcer `powerline.bash`.

`POWERLINE_ICONS` le style d'icônes à utiliser pour les séparateur de segments
comme pour les segments eux-même. Au choix parmi `compat`, `powerline` (par
défaut), `flat`, `icons-in-terminal` et `nerd-fonts`.

Le changement de `POWERLINE_ICONS` exige le rechargement du shell.


## Segments

Un segment est l'équivalent d'une extension de votre shell, il affiche un texte
coloré et eventuellement une icône. Voici la liste des segments disponibles:

- `hostname` - nom de l'utilisateur et nom de la machine. Par défaut, activé si
  en SSH, docker, LXC ou sudo.

  ![Segment hostname](docs/segment-hostname.png)

- `pwd` - dossier de travail courant, raccourcis. Activé par défaut.

  ![Segment pwd](docs/segment-pwd.png)

- `status` - code de retour d'erreur de la précédente commande. Activé par
  défaut.

  ![Segment status](docs/segment-status.png)

- `python` - nom du virtualenv, environment Conda ou version Pyenv. Par défaut,
  activité si python est installé.

  ![Segment python](docs/segment-python.png)

- `git` - branche courante, état du dossier de travail et de la synchronisation
  distante de git. Activité si git est installé.

  ![Segment git](docs/segment-git.png)

- `maildir` - présence d'un email non-lu dans le répertoire surveillé. Activé
  si la variable d'environnement `POWERLINE_MAILDIR` est définie.

  ![Segment maildir](docs/segment-maildir.png)

- `openstack` - identifiant et projet ou accès API et URL d'un fichier `openrc`
  pour OpenStack. Activité si python est installé.

  ![Segment openstack](docs/segment-openstack.png)

- `docker` - état du projet compose : nombre de services démarrés sur le nombre
  total de service du fichier compose. Activé si un docker-compose.yml existe.

  ![Segment docker](docs/segment-docker.png)

- `k8s` - namespace kubernetes courant. Activé si kubectl est installé et si le
  fichier de configuration `~/.kube/config` ou celui défini par `KUBECONFIG`
  existe.

    avec POWERLINE_K8S_CTX_SHOW=0

  ![Segment k8s-0](docs/segment-k8s-0.png)

    avec POWERLINE_K8S_CTX_SHOW=1

  ![Segment k8s-1](docs/segment-k8s-1.png)


Pour supprimer un segment autosélectionné à postériori, éditer
`POWERLINE_SEGMENTS` après avoir sourcé `powerline.bash`.

``` bash
. ~/.local/lib/powerline.bash
POWERLINE_SEGMENTS=${POWERLINE_SEGMENTS/nom_du_segment}
```


## Compatibilité avec les polices

Si vous utilisez une police sans les symboles Powerline, vous pouvez configurer
l'invite pour avoir un mode dégradé avec `POWERLINE_ICONS=compat` ou carrément
`POWERLINE_ICONS=flat` avant de sourcer `powerline.bash` dans `.bashrc`.


## Auteurs

- [Étienne BERSAC](https://gitlab.com/bersace) - mainteneur
- [Frédéric MEYNADIER](https://gitlab.com/fmeynadier) - segment maildir,
  support pyenv
- [Olivier AUDRY](https://gitlab.com/olivier.audry) - segment k8s
- [Sandile KESWA](https://github.com/skeswa) - projet original dont dérive
  powerline.bash

Les contributions sont bienvenues, quelque soit votre niveau de BASH. Un
segment partagé est optimisé et contient moins d'erreur !


## Références

- https://github.com/b-ryan/powerline-shell
- https://github.com/ramnes/context-color
- https://github.com/skeswa/prompt
