#!/bin/bash -eu

LOGNAME=user
HOSTNAME=host
. ./powerline.bash

for RGB in {0..5}{0..5}{0..5} ; do
    DEBUG=o __powerline_get_foreground ${RGB::1} ${RGB:1:1} ${RGB:2:1}
    echo
done
