#!/bin/bash -eux

. ./powerline.bash

pushd ${PWD_OVERWRITE-${PWD}}
time __update_ps1 ${EXIT_CODE-1}

set +x

echo "------------------>8----------------"
echo
echo "$PS1"
echo

# Nettoyer les \[\]
PS1="${PS1//\\]/}"
PS1="${PS1//\\[/}"
# Remplacer \$ par $
PS1="${PS1/\\\$/$}"

echo -e "${PS1}"
echo
echo "------------------8<----------------"
